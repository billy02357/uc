uc
--
CLI unit converter

Units supported (for now):
- Fahrenheit to Kelvin and Celsius and vice versa
- Pounds to Kilograms and vice versa

Usage
-----
```sh
$ uc -h
uc: Unit converter
Usage: uc [option] [values]
Options:
	-h, --help	Print this help message
Temperature:
	-cf		Take input as Celsius and output as Fahrenheit
	-ck		Take input as Celsius and output as Kelvin
	-fc		Take input as Fahrenheit and output as Celsius
	-fk		Take input as Fahrenheit and output as Kelvin
	-kc		Take input as Kelvin and output as Celsius
	-kf		Take input as Kelvin and output as Fahrenheit
Weight:
	-pkg		Take input as Pounds and output as Kilograms
	-kgp		Take input as Kilograms and output as Pounds
Length:
	-mmi		Take input as Meters and output as Miles
	-mim		Take input as Miles and output as Meters
	-min		Take input as Meters and output as Inches
	-m_mm		Take input as Meters and output as Millimeters
	-mcm		Take input as Meters and output as Centimeters
	-mft		Take input as Meters and output as Feet
	-cmm		Take input as Centimeters and output as Meters
	-cmmm		Take input as Centimeters and output as Millimeters
	-cmmi		Take input as Centimeters and output as Miles
	-cmin		Take input as Centimeters and output as Inches
	-cmft		Take input as Centimeters and output as Feet
	-mm_m		Take input as Millimeters and output as Meters
	-mmcm		Take input as Millimeters and output as Centimeters


Author(s):
Biel Sala (bielsalamimo@gmail.com)
```
Examples:
```sh
$ uc cf 100 # Convert 100 degrees Celsius to Fahrenheit
212.000000
$ uc fc 212 # Convert 212 degrees Fahrenheit to Celsius
100.000000
$ uc ck 0 # Convert 0 degrees Celsius to Kelvin
273.150000
$ uc kf 273.15 # Convert 273.15 degrees Kelvin to Fahrenheit
32.000000
$ uc fc 32 # Convert 32 degrees Fahrenheit to Celsius
0.000000
```
