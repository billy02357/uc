package main

import (
	"fmt"
	"os"

	"github.com/billy02357/conv"
)

func printUsage() {
	fmt.Printf(`uc: Unit converter
Usage: uc [option] [values]
Options:
	-h, --help	Print this help message
Temperature:
	-cf		Take input as Celsius and output as Fahrenheit
	-ck		Take input as Celsius and output as Kelvin
	-fc		Take input as Fahrenheit and output as Celsius
	-fk		Take input as Fahrenheit and output as Kelvin
	-kc		Take input as Kelvin and output as Celsius
	-kf		Take input as Kelvin and output as Fahrenheit
Weight:
	-pkg		Take input as Pounds and output as Kilograms
	-kgp		Take input as Kilograms and output as Pounds
Length:
	-mmi		Take input as Meters and output as Miles
	-mim		Take input as Miles and output as Meters
	-min		Take input as Meters and output as Inches
	-m_mm		Take input as Meters and output as Millimeters
	-mcm		Take input as Meters and output as Centimeters
	-mft		Take input as Meters and output as Feet
	-cmm		Take input as Centimeters and output as Meters
	-cmmm		Take input as Centimeters and output as Millimeters
	-cmmi		Take input as Centimeters and output as Miles
	-cmin		Take input as Centimeters and output as Inches
	-cmft		Take input as Centimeters and output as Feet
	-mm_m		Take input as Millimeters and output as Meters
	-mmcm		Take input as Millimeters and output as Centimeters
	-mmmi		Take input as Millimeters and output as Miles
	-mmin		Take input as Millimeters and output as Inches
	-mmft		Take input as Millimeters and output as Feet


Author(s):
Biel Sala (bielsalamimo@gmail.com)
`)
	os.Exit(1)
}

func main() {

	switch {
	case isNoArgs(), isHelp():
		printUsage()

	/* CELSIUS TO FAHRENHEIT */
	case isCF():
		c := parseNumber()
		f := conv.CToF(conv.Celsius(c))
		fmt.Println(f.String())

	/* FAHRENHEIT TO CELSIUS */
	case isFC():
		f := parseNumber()
		c := conv.FToC(conv.Fahrenheit(f))
		fmt.Println(c.String())

	/* CELSIUS TO KELVIN */
	case isCK():
		c := parseNumber()
		k := conv.CToK(conv.Celsius(c))
		fmt.Println(k.String())

	/* KELVIN TO CELSIUS */
	case isKC():
		k := parseNumber()
		c := conv.KToC(conv.Kelvin(k))
		fmt.Println(c.String())

	/* KELVIN TO FAHRENHEIT */
	case isKF():
		k := parseNumber()
		f := conv.KToF(conv.Kelvin(k))
		fmt.Println(f.String())

	/* FAHRENHEIT TO KELVIN */
	case isFK():
		f := parseNumber()
		k := conv.FToK(conv.Fahrenheit(f))
		fmt.Println(k.String())

	/* POUNDS TO KILOGRAMS */
	case isPKG():
		p := parseNumber()
		kg := conv.PToKG(conv.Pound(p))
		fmt.Printf("%f\n", kg)

	/* KILOGRAMS TO POUNDS */
	case isKGP():
		kg := parseNumber()
		p := conv.KGToP(conv.Kilogram(kg))
		fmt.Printf("%f\n", p)

	/* METERS TO MILES */
	case isMMI():
		m := parseNumber()
		mi := conv.MToMI(conv.Meter(m))
		fmt.Printf("%f\n", mi)

	/* MILES TO METERS */
	case isMIM():
		mi := parseNumber()
		m := conv.MIToM(conv.Mile(mi))
		fmt.Printf("%f\n", m)

	/* METERS TO INCHES */
	case isMIN():
		m := parseNumber()
		in := conv.MToIN(conv.Meter(m))
		fmt.Printf("%f\n", in)

	/* METERS TO MILLIMETERS */
	case isM_MM():
		m := parseNumber()
		mm := conv.MToMM(conv.Meter(m))
		fmt.Printf("%f\n", mm)

	/* METERS TO CENTIMETERS */
	case isMCM():
		m := parseNumber()
		cm := conv.MToCM(conv.Meter(m))
		fmt.Printf("%f\n", cm)

	/* METERS TO FEET */
	case isMFT():
		m := parseNumber()
		ft := conv.MToFT(conv.Meter(m))
		fmt.Printf("%f\n", ft)

	/* CENTIMETERS TO METERS */
	case isCMM():
		cm := parseNumber()
		m := conv.CMToM(conv.Centimeter(cm))
		fmt.Printf("%f\n", m)

	/* CENTIMETER TO MILLIMETER */
	case isCMMM():
		cm := parseNumber()
		mm := conv.CMToMM(conv.Centimeter(cm))
		fmt.Printf("%f\n", mm)

	/* CENTIMETER TO MILE */
	case isCMMI():
		cm := parseNumber()
		mi := conv.CMToMI(conv.Centimeter(cm))
		fmt.Printf("%f\n", mi)

	/* CENTIMETER TO INCH */
	case isCMIN():
		cm := parseNumber()
		in := conv.CMToIN(conv.Centimeter(cm))
		fmt.Printf("%f\n", in)

	/* CENTIMETER TO FEET */
	case isCMFT():
		cm := parseNumber()
		ft := conv.CMToFT(conv.Centimeter(cm))
		fmt.Printf("%f\n", ft)

	/* MILLIMETER TO METER */
	case isMM_M():
		mm := parseNumber()
		m := conv.MMToM(conv.Millimeter(mm))
		fmt.Printf("%f\n", m)

	/* MILLIMETER TO CENTIMETER */
	case isMMCM():
		mm := parseNumber()
		cm := conv.MMToCM(conv.Millimeter(mm))
		fmt.Printf("%f\n", cm)

	/* MILLIMETER TO MILES */
	case isMMMI():
		mm := parseNumber()
		mi := conv.MMToMI(conv.Millimeter(mm))
		fmt.Printf("%f\n", mi)

	/* MILLIMETER TO INCHES */
	case isMMIN():
		mm := parseNumber()
		in := conv.MMToIN(conv.Millimeter(mm))
		fmt.Printf("%f\n", in)

	/* MILLIMETER TO FEET */
	case isMMFT():
		mm := parseNumber()
		ft := conv.MMToFT(conv.Millimeter(mm))
		fmt.Printf("%f\n", ft)

	}
}
