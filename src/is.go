package main

import (
	"log"
	"os"
	"strconv"
)

func isNoArgs() bool {
	if len(os.Args) == 1 {
		return true
	} else {
		return false
	}
}

func isHelp() bool {
	if os.Args[1] == "h" || os.Args[1] == "help" || os.Args[1] == "-h" || os.Args[1] == "-help" || os.Args[1] == "--h" || os.Args[1] == "--help" {
		return true
	} else {
		return false
	}
}

func parseNumber() float64 {
	num, err := strconv.ParseFloat(os.Args[2], 64)
	if err != nil {
		log.Fatal(err)
	}
	return num
}

func isCF() bool {
	if os.Args[1] == "-cf" || os.Args[1] == "cf" {
		return true
	} else {
		return false
	}
}

func isFC() bool {
	if os.Args[1] == "-fc" || os.Args[1] == "fc" {
		return true
	} else {
		return false
	}
}

func isCK() bool {
	if os.Args[1] == "-ck" || os.Args[1] == "ck" {
		return true
	} else {
		return false
	}
}

func isKC() bool {
	if os.Args[1] == "-kc" || os.Args[1] == "kc" {
		return true
	} else {
		return false
	}
}

func isKF() bool {
	if os.Args[1] == "-kf" || os.Args[1] == "kf" {
		return true
	} else {
		return false
	}
}

func isFK() bool {
	if os.Args[1] == "-fk" || os.Args[1] == "fk" {
		return true
	} else {
		return false
	}
}

func isPKG() bool {
	if os.Args[1] == "-pkg" || os.Args[1] == "pkg" {
		return true
	} else {
		return false
	}
}

func isKGP() bool {
	if os.Args[1] == "-kgp" || os.Args[1] == "kgp" {
		return true
	} else {
		return false
	}
}

func isMMI() bool {
	if os.Args[1] == "-mmi" || os.Args[1] == "mmi" {
		return true
	} else {
		return false
	}
}

func isMIM() bool {
	if os.Args[1] == "-mim" || os.Args[1] == "mim" {
		return true
	} else {
		return false
	}
}

func isMIN() bool {
	if os.Args[1] == "-min" || os.Args[1] == "min" {
		return true
	} else {
		return false
	}
}

func isM_MM() bool {
	if os.Args[1] == "-m_mm" || os.Args[1] == "m_mm" {
		return true
	} else {
		return false
	}
}

func isMCM() bool {
	if os.Args[1] == "-mcm" || os.Args[1] == "mcm" {
		return true
	} else {
		return false
	}
}

func isMFT() bool {
	if os.Args[1] == "-mft" || os.Args[1] == "mft" {
		return true
	} else {
		return false
	}
}

func isCMM() bool {
	if os.Args[1] == "-cmm" || os.Args[1] == "cmm" {
		return true
	} else {
		return false
	}
}

func isCMMM() bool {
	if os.Args[1] == "-cmmm" || os.Args[1] == "cmmm" {
		return true
	} else {
		return false
	}
}

func isCMMI() bool {
	if os.Args[1] == "-cmmi" || os.Args[1] == "cmmi" {
		return true
	} else {
		return false
	}
}

func isCMIN() bool {
	if os.Args[1] == "-cmin" || os.Args[1] == "cmin" {
		return true
	} else {
		return false
	}
}

func isCMFT() bool {
	if os.Args[1] == "-cmft" || os.Args[1] == "cmft" {
		return true
	} else {
		return false
	}
}

func isMM_M() bool {
	if os.Args[1] == "-mm_m" || os.Args[1] == "mm_m" {
		return true
	} else {
		return false
	}
}

func isMMCM() bool {
	if os.Args[1] == "-mmcm" || os.Args[1] == "mmcm" {
		return true
	} else {
		return false
	}
}

func isMMMI() bool {
	if os.Args[1] == "-mmmi" || os.Args[1] == "mmmi" {
		return true
	} else {
		return false
	}
}

func isMMIN() bool {
	if os.Args[1] == "-mmin" || os.Args[1] == "mmin" {
		return true
	} else {
		return false
	}
}

func isMMFT() bool {
	if os.Args[1] == "-mmft" || os.Args[1] == "mmft" {
		return true
	} else {
		return false
	}
}
